﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCD
{
    class ImageProcessing
    {
        public static readonly Color Red = Color.FromArgb(255, 0, 0);
        public static readonly Color Green = Color.FromArgb(0, 255, 0);
        public static readonly Color Blue = Color.FromArgb(0, 0, 255);
        public static readonly Color White = Color.FromArgb(255, 255, 255);
        public static readonly Color Black = Color.FromArgb(0, 0, 0);

        public enum FlipType
        {
            Horizontal = 1,
            Vertikal = 2
        };

        public static Bitmap Grayscale(Bitmap bmp)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = (tmp.GetPixel(x, y).R + tmp.GetPixel(x, y).G + tmp.GetPixel(x, y).B) / 3;
                tmp.SetPixel(x, y, Color.FromArgb(m, m, m));
            }
            return tmp;
        }

        public static Bitmap Binerize(Bitmap bmp, int threshold)
        {
            Bitmap tmp = Grayscale(bmp);
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = tmp.GetPixel(x, y).R;
                m = m < threshold ? 0 : 255;
                tmp.SetPixel(x, y, Color.FromArgb(m, m, m));
            }
            return tmp;
        }

        public static Bitmap Brightness(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (tmp.GetPixel(x, y).R + k) > 255 ? 255 : (tmp.GetPixel(x, y).R + k);
                int g = (tmp.GetPixel(x, y).G + k) > 255 ? 255 : (tmp.GetPixel(x, y).G + k);
                int b = (tmp.GetPixel(x, y).B + k) > 255 ? 255 : (tmp.GetPixel(x, y).B + k);
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap Flip(Bitmap bmp, FlipType ft)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            Debug.WriteLine("Height : " + bmp.Height);
            Debug.WriteLine("Width : " + bmp.Width);
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                Color c = new Color();
                switch (ft)
                {
                    case FlipType.Horizontal:
                        c = bmp.GetPixel(bmp.Width - 1 - x, y);
                        break;
                    case FlipType.Vertikal:
                        c = bmp.GetPixel(x, bmp.Height - 1 - y);
                        break;
                }
                tmp.SetPixel(x, y, Color.FromArgb(c.R, c.G, c.B));
            }
            return tmp;
        }

        public static Bitmap Kuantisasi(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            int th = 256 / k;
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = (tmp.GetPixel(x, y).R + tmp.GetPixel(x, y).G + tmp.GetPixel(x, y).B) / 3;
                int q = th * (m / th);
                tmp.SetPixel(x, y, Color.FromArgb(q, q, q));
            }
            return tmp;
        }

        public static Bitmap Contrast(Bitmap bmp, float k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) ((tmp.GetPixel(x, y).R * k) > 255 ? 255 : (tmp.GetPixel(x, y).R * k));
                int g = (int) ((tmp.GetPixel(x, y).G * k) > 255 ? 255 : (tmp.GetPixel(x, y).G * k));
                int b = (int) ((tmp.GetPixel(x, y).B * k) > 255 ? 255 : (tmp.GetPixel(x, y).B * k));
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap Negative(Bitmap bmp)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
                tmp.SetPixel(x, y,
                    Color.FromArgb(255 - tmp.GetPixel(x, y).R, 255 - tmp.GetPixel(x, y).G, 255 - tmp.GetPixel(x, y).B));
            return tmp;
        }

        public static Bitmap Logaritmic(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) (k * Math.Log(tmp.GetPixel(x, y).R, 10));
                int g = (int) (k * Math.Log(tmp.GetPixel(x, y).G, 10));
                int b = (int) (k * Math.Log(tmp.GetPixel(x, y).B, 10));
                r = r > 255 ? 255 : r;
                g = g > 255 ? 255 : g;
                b = b > 255 ? 255 : b;
                r = r < 0 ? 0 : r;
                g = g < 0 ? 0 : g;
                b = b < 0 ? 0 : b;
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap InversLog(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) (10 * Math.Exp(tmp.GetPixel(x, y).R / k));
                int g = (int) (10 * Math.Exp(tmp.GetPixel(x, y).G / k));
                int b = (int) (10 * Math.Exp(tmp.GetPixel(x, y).B / k));
                r = r > 255 ? 255 : r;
                g = g > 255 ? 255 : g;
                b = b > 255 ? 255 : b;
                r = r < 0 ? 0 : r;
                g = g < 0 ? 0 : g;
                b = b < 0 ? 0 : b;
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap Power(Bitmap bmp, float k, float k2)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) (k * tmp.GetPixel(x, y).R * Math.Exp(k2));
                int g = (int) (k * tmp.GetPixel(x, y).G * Math.Exp(k2));
                int b = (int) (k * tmp.GetPixel(x, y).B * Math.Exp(k2));
                r = r > 255 ? 255 : r;
                g = g > 255 ? 255 : g;
                b = b > 255 ? 255 : b;
                r = r < 0 ? 0 : r;
                g = g < 0 ? 0 : g;
                b = b < 0 ? 0 : b;
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap RootPower(Bitmap bmp, float k, float k2)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) (k * tmp.GetPixel(x, y).R * Math.Exp(1 / k2));
                int g = (int) (k * tmp.GetPixel(x, y).G * Math.Exp(1 / k2));
                int b = (int) (k * tmp.GetPixel(x, y).B * Math.Exp(1 / k2));
                r = r > 255 ? 255 : r;
                g = g > 255 ? 255 : g;
                b = b > 255 ? 255 : b;
                r = r < 0 ? 0 : r;
                g = g < 0 ? 0 : g;
                b = b < 0 ? 0 : b;
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static void GenHistogram(Bitmap bmp, int[] red, int[] green, int[] blue)
        {
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = bmp.GetPixel(x, y).R;
                int g = bmp.GetPixel(x, y).G;
                int b = bmp.GetPixel(x, y).B;
                red[r]++;
                green[g]++;
                blue[b]++;
            }
        }

        public static Bitmap MergeImage(Bitmap bmp1, Bitmap bmp2, float op1, float op2)
        {
            Bitmap tmp1 = (Bitmap) bmp1.Clone();
            Bitmap tmp2 = (Bitmap) bmp2.Clone();
            int width = Max(tmp1.Width, tmp2.Width);
            int height = Max(tmp1.Height, tmp2.Height);
            Bitmap bmp = new Bitmap((Bitmap) tmp1.Clone(), width, height);
            Color c1, c2;
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r, g, b, r1, g1, b1;
                r = g = b = r1 = g1 = b1 = 0;
                try
                {
                    c1 = tmp1.GetPixel(x, y);
                    r = c1.R;
                    g = c1.G;
                    b = c1.G;
                }
                catch (Exception e)
                {
                    // ignored
                }
                try
                {
                    c2 = tmp2.GetPixel(x, y);
                    r1 = c2.R;
                    g1 = c2.G;
                    b1 = c2.G;
                }
                catch (Exception e)
                {
                    // ignored
                }
                r = (int) ((op1 * r + op2 * r1) / 2);
                g = (int) ((op1 * g + op2 * g1) / 2);
                b = (int) ((op1 * b + op2 * b1) / 2);
                bmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return bmp;
        }

        private static int Max(int i1, int i2)
        {
            return i1 > i2 ? i1 : i2;
        }

        public static Bitmap StaticColorDetection(Bitmap bmp, int[] redRange, int[] greenRange, int[] blueRange)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            Color c;
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                c = tmp.GetPixel(x, y);
                int r = c.R;
                int g = c.G;
                int b = c.B;
                if (r < redRange[0] || r > redRange[1] || g < greenRange[0] || g > greenRange[1] || b < blueRange[0] ||
                    b > blueRange[1])
                {
                    r = 0;
                    g = 0;
                    b = 0;
                }
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap DistanceColorDetection(Bitmap bmp, int redVal, int greenVal, int blueVal, int distance)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            Color c;
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                c = tmp.GetPixel(x, y);
                int r = c.R;
                int g = c.G;
                int b = c.B;
                int d = Math.Abs(r - redVal) + Math.Abs(g - greenVal) + Math.Abs(b - blueVal);
                if (d > distance)
                {
                    r = 0;
                    g = 0;
                    b = 0;
                }
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap Konvolusi(Bitmap bmp, int[][] mask)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            tmp = Grayscale(tmp);
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int v = 0;
                int d = mask.Length / 2;
                for (int i = 0; i < mask.Length; i++)
                for (int j = 0; j < mask[i].Length; j++)
                {
                    int a = (x + j - d) < 0 ? 0 : (x + j - d);
                    int b = (y + i - d) < 0 ? 0 : (y + i - d);
                    int c = tmp.GetPixel(a, b).R;
                    v += (c * mask[j][i]);
                }
                tmp.SetPixel(x, y, Color.FromArgb(v, v, v));
            }
            return tmp;
        }
    }
}